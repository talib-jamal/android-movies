package mt.movies.utility;


import java.util.HashMap;

import mt.movies.retrofit.RetrofitFactory;

/**
 * Created by talib on 2/27/2017.
 */

public class FrontEngine {

    public static FrontEngine frontEngine = null;
    public String MainApiUrl = "https://api.themoviedb.org/3/";
    public String accessToken = "";
    public float dpi;
    private RetrofitFactory retrofitFactory;

    public static FrontEngine getInstance() {
        if (frontEngine == null)
            frontEngine = new FrontEngine();
        return frontEngine;
    }

    public RetrofitFactory getRetrofitFactory() {
        retrofitFactory = new RetrofitFactory();
        return retrofitFactory;
    }


    public HashMap getMap(String[] strings) {
        HashMap<String, String> map = new HashMap<>();
        if (strings != null && strings.length > 1) {
            for (int i = 0; i < strings.length; i += 2)
                map.put(strings[i], strings[i + 1]);
        }
        return map;
    }


}

