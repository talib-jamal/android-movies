package mt.movies.interfaces;

import mt.movies.models.Movies;

/**
 * Created by Talib on 21/09/2017.
 */

public interface ListItemClick {

    public void itemClick(Movies movie);

}
