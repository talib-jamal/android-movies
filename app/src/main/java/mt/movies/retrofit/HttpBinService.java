package mt.movies.retrofit;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by talib on 1/20/2017.
 */
public interface HttpBinService {


    @POST("{url}")
    Call<JsonElement> postData(@Path(value = "url", encoded = true) String url, @HeaderMap HashMap<String, String> header, @Body JsonObject jsonObject);

    @PUT
    Call<JsonElement> putData(@Url String url, @HeaderMap HashMap<String, String> header, @Body JsonObject jsonObject);

    @GET
    Call<JsonElement> getData(@Url String url, @HeaderMap HashMap<String, String> header);

    @POST("{url}")
    @Multipart
    Call<JsonElement> postProfilePicture(@Path("url") String url, @HeaderMap HashMap<String, String> header,
                                         @Part MultipartBody.Part image, @Part("share_with") RequestBody[] name);

    //@DELETE
    @HTTP(method = "DELETE", /*path = "post/delete",*/ hasBody = true)
    Call<JsonElement> deleteData(@Url String url, @HeaderMap HashMap<String, String> header, @Body JsonObject jsonObject);


}
