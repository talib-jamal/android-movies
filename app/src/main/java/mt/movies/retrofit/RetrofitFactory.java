package mt.movies.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import mt.movies.utility.FrontEngine;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by talib on 1/20/2017.
 */

public class RetrofitFactory {
    public static final int POST = 1, GET = 2, PUT = 3, DELETE = 4;
    Retrofit retrofit;
    Call<JsonElement> j = null;
    HttpBinService service = null;

    public void requestService(int methodType, HashMap map, String postURL, JsonObject jsonElement, CallBackRetrofit callback) {
        HttpBinService service = getServiceInstance(postURL + "/");

        if (methodType == POST)
            j = service.postData(postURL, map, jsonElement);
        else if (methodType == PUT)
            j = service.putData(postURL, map, jsonElement);
        else if (methodType == GET)
            j = service.getData(postURL, map);
        else if (methodType == DELETE)
            j = service.deleteData(postURL, map, jsonElement);

        j.enqueue(callback);
    }


    public HttpBinService getServiceInstance(String postURL) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
        okHttpClient.connectTimeout(1, TimeUnit.MINUTES);
        okHttpClient.readTimeout(1, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(1, TimeUnit.MINUTES);
        okHttpClient.followRedirects(false);
        okHttpClient.followSslRedirects(false);
        OkHttpClient okHttpClient_ = okHttpClient.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(FrontEngine.getInstance().MainApiUrl)
                .client(okHttpClient_)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(HttpBinService.class);


        return service;
    }




    public void cancelRequest() {
        j.cancel();
    }

    public boolean isExceutedCall() {
        if (j.isExecuted()) {
            return true;
        } else {
            return false;
        }
    }

}
