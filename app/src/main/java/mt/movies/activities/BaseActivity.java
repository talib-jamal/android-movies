package mt.movies.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import mt.movies.R;
import mt.movies.dialogs.CustomDialog;
import mt.movies.dialogs.SpinnerDialog;

public class BaseActivity extends AppCompatActivity {

    protected BaseActivity activity;
    protected String apiKey = "028fe29c4e2311ec84a005de2ac11885";
    protected String access_token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIwMjhmZTI5YzRlMjMxMWVjODRhMDA1ZGUyYWMxMTg4NSIsInN1YiI6IjU5YzI2NjdiOTI1MTQxNzZkYzAwZWFhNyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.N3G0Q7WrHkYg-EXW4v4kkVp8LMTuYd0qCKFIRtOpwW4";

    protected String movieUrl = "movie/upcoming?language=en-US&api_key="+apiKey;
    protected String searchMovieUrl = "discover/movie?api_key="+apiKey+"&sort_by=vote_average.desc&primary_release_year=";
    protected String detailMovieUrl = "?api_key=028fe29c4e2311ec84a005de2ac11885&language=en-US";
    public String imagePath = "https://image.tmdb.org/t/p/w500/";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    //activity change function start
    public void startNewActivity(Activity activity, Class className, int flags, int animIN, int animOut, Bundle b, boolean stratFromResult, int INTENT_CODE) {
        Intent i = new Intent(activity, className);
        i.addFlags(flags);
        i.putExtras(b);
        if (stratFromResult)
            activity.startActivityForResult(i, INTENT_CODE);
        else
            activity.startActivity(i);
        activity.overridePendingTransition(animIN, animOut);

    }
    //activity change function close

    //Spinner dialog start from here
    public void spinnerStart(SpinnerDialog spinnerDialog) {
        if (spinnerDialog != null && !spinnerDialog.isShowing()) {
            spinnerDialog.setCancelable(false);
            spinnerDialog.setCanceledOnTouchOutside(false);
            spinnerDialog.show();
        }
    }
    //Spinner dialog end

    public void spinnerStop(SpinnerDialog spinnerDialog) {
        if (spinnerDialog != null)
            spinnerDialog.dismiss();
    }

    public CustomDialog dialog;

    //call create event dialog function start
    public void callDialog(Activity activity, String title, String desc) {
        dialog = new CustomDialog(activity, title, desc, new CustomDialog.OnClickSelection() {
            @Override
            public void setOk() {
                if (dialog != null)
                    dialog.dismiss();

            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
        }

    }
    //call create event dialog function close

    //setting image on glide start
    public void setImageOnGlide(String url, final ImageView imageView, final ProgressBar progressBar) {
        Glide.with(activity)
                .load(url)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {

                        progressBar.setVisibility(View.GONE);
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(activity.getResources(), bitmap);
                        imageView.setImageDrawable(circularBitmapDrawable);

                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        progressBar.setVisibility(View.GONE);
                        imageView.setImageResource(R.color.image_load);

                    }

                    @Override
                    public void onLoadStarted(Drawable placeholder) {
                        super.onLoadStarted(placeholder);
                        imageView.setImageResource(R.color.image_load);
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });
    }

    //setting image on glide close


}
