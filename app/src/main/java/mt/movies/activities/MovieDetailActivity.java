package mt.movies.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import mt.movies.R;
import mt.movies.dialogs.SpinnerDialog;
import mt.movies.models.MovieDetail;

public class MovieDetailActivity extends BaseActivity {

    private MovieDetail movieDetail;
    private ImageView ivMoviePic;
    private ProgressBar pb;
    private TextView tvTitle, tvSubtitle, tvReleaseDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        Gson gson = new Gson();

        if(getIntent().getStringExtra(getString(R.string.movie_data))!=null)
        movieDetail = gson.fromJson(getIntent().getStringExtra(getString(R.string.movie_data)).toString(), MovieDetail.class);

        initilizingViews();

    }
    //onCreate close

    //initilizing views start
    public void initilizingViews() {

        ivMoviePic = (ImageView)findViewById(R.id.ivMoviePic);
        pb = (ProgressBar)findViewById(R.id.pb);

        //setting movie cover picture
        setImageOnGlide(activity.imagePath+movieDetail.getPosterPath(),
                ivMoviePic,
                pb);

        tvTitle = (TextView)findViewById(R.id.tvTitle);
        tvTitle.setText("Title:\n"+movieDetail.getTitle());

        tvSubtitle = (TextView)findViewById(R.id.tvSubtitle);
        tvSubtitle.setText("Description:\n"+movieDetail.getOverview());

        tvReleaseDate = (TextView)findViewById(R.id.tvReleaseDate);
        tvReleaseDate.setText("Release Date:\n"+movieDetail.getReleaseDate());


    }
    //initilizing views close



}
