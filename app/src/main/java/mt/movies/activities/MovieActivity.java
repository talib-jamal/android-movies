package mt.movies.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import mt.movies.R;
import mt.movies.adapters.MovieAdapter;
import mt.movies.dialogs.SpinnerDialog;
import mt.movies.interfaces.ListItemClick;
import mt.movies.internet.CheckInternet;
import mt.movies.models.ModelParse;
import mt.movies.models.MovieDetail;
import mt.movies.models.Movies;
import mt.movies.retrofit.CallBackRetrofit;
import mt.movies.retrofit.HttpResponse;
import mt.movies.retrofit.RetrofitFactory;
import mt.movies.retrofit.ServiceResponse;
import mt.movies.utility.FrontEngine;

public class MovieActivity extends BaseActivity implements View.OnClickListener, ServiceResponse, ListItemClick{

    public static TextView tvDate;
    private Button btFilter;
    private RecyclerView rvMovies;
    private MovieAdapter movieAdapter;
    private List<Movies> moviesList = new ArrayList<>();
    private List<Movies> searchMoviesList = new ArrayList<>();
    private SpinnerDialog spinnerDialog = null;
    private ModelParse modelParsedResponse;
    private final int MOVIES_LIST = 1, MOVIES_LOAD = 2, MOVIES_FILTER = 3, MOVIE_DETAIL = 4;
    private int page = 1;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager mLayoutManager;
    private int searchPage = 1;
    private boolean search = false;
    private MovieDetail movieDetailParse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);


        initilizingViews();


    }
    //onCreate close

    //initilizing views start
    public void initilizingViews() {

        tvDate = (TextView) findViewById(R.id.tvDate);
        tvDate.setOnClickListener(this);

        btFilter = (Button) findViewById(R.id.btFilter);
        btFilter.setOnClickListener(this);

        setRecycleViewCategory();

        spinnerDialog = new SpinnerDialog(activity);

        requestHttpCall(MOVIES_LIST, movieUrl + "&page=" + page);


    }
    //initilizing views close


    //setting recycle views start for list of movies start
    public void setRecycleViewCategory() {

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvMovies = (RecyclerView) findViewById(R.id.rvMovies);
        rvMovies.setLayoutManager(mLayoutManager);

        setLoadMoreMovies();

    }
    //setting recycle views close

    //movies list adapter start
    public void setMovieAdapter(List<Movies> array) {
        moviesList.addAll(array);
        movieAdapter = new MovieAdapter(activity, moviesList, this);
        rvMovies.setAdapter(movieAdapter);
    }
    //movies list adapter close

    //movies list adapter start
    public void setMovieSearchAdapter(List<Movies> array) {
        searchMoviesList.addAll(array);
        movieAdapter = new MovieAdapter(activity, searchMoviesList, this);
        rvMovies.setAdapter(movieAdapter);
    }
    //movies list adapter close

    //load more data in recycle view start
    public void setLoadMoreMovies() {

        rvMovies.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                //loading data false when list has no data on server
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {

                    if(!search){

                        visibleItemCount = mLayoutManager.getChildCount();
                        totalItemCount = mLayoutManager.getItemCount();
                        pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                        if (visibleItemCount + pastVisiblesItems == totalItemCount) {

                            if (modelParsedResponse.getTotalPages() >= 5) {

                                //check total pages is equal to current page
                                if (modelParsedResponse.getPage() != modelParsedResponse.getTotalPages()) {

                                    //checking last result greater than 0 then load
                                    //because some times server gives total pages max but
                                    //in reponse which we are getting result array is null coming from server
                                    if(modelParsedResponse.getResults().size()>0){

                                        if(!spinnerDialog.isShowing()){
                                            page = modelParsedResponse.getPage()+1;
                                            requestHttpCall(MOVIES_LOAD, movieUrl + "&page=" + page);
                                        }

                                    }

                                }

                            }



                        }

                    }
                    //search boolean check close



                }
                //scroll state check close


            }

        });

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tvDate: {

                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");

                break;
            }
            //select date case close


            case R.id.btFilter: {
                requestHttpCall(MOVIES_FILTER, searchMovieUrl + Integer.parseInt(tvDate.getTag().toString())+"&page=" + searchPage);
                break;
            }
            //search by filter case close

        }//switch close

    }

    //onclick overide close

    @Override
    public void itemClick(Movies movie) {

        requestHttpCall(MOVIE_DETAIL, "movie/"+movie.getId()+detailMovieUrl);

    }
    //onlist item click close


    //date picker start
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            tvDate.setText("" + day + "/" + (month + 1) + "/" + year);
            tvDate.setTag(""+year);
        }
    }
    //date picker close

    @Override
    public void onResult(int type, HttpResponse o) {

        spinnerStop(spinnerDialog);

        if (modelParsedResponse != null) {

            if (o.getResponseCode() == 200) {

                switch (type) {
                    case MOVIES_LIST: {
                        setMovieAdapter(modelParsedResponse.getResults());
                        break;
                    }

                    case MOVIES_LOAD: {

                        if (modelParsedResponse.getResults().size() > 0) {

                            rvMovies.smoothScrollToPosition(moviesList.size()-1);
                            moviesList.addAll(modelParsedResponse.getResults());
                            movieAdapter.notifyDataSetChanged();


                        } else {
                            callDialog(activity, getString(R.string.alert), getString(R.string.load_more));
                        }

                        break;
                    }

                    case MOVIES_FILTER: {
                        search = true;
                        setMovieSearchAdapter(modelParsedResponse.getResults());
                        break;
                    }

                    case MOVIE_DETAIL: {
                        if (modelParsedResponse != null) {
                            Bundle bundle = new Bundle();
                            Gson gson = new Gson();
                            bundle.putString(getString(R.string.movie_data), gson.toJson(movieDetailParse));

                            startNewActivity(MovieActivity.this,
                                    MovieDetailActivity.class,
                                    0,
                                    0,
                                    0,
                                    bundle,
                                    false,
                                    0);
                        }
                        break;
                    }

                }


            } else {
                customDialogSomethingWent();
            }

        } else {
            customDialogSomethingWent();
        }


    }

    public void customDialogSomethingWent() {
        callDialog(activity, getString(R.string.request_failed), getString(R.string.something_wentwrong));
    }


    @Override
    public void parseDataInBackground(int type, HttpResponse o) {

        if (o != null && !TextUtils.isEmpty(o.getResponseData())) {

            switch (type) {
                case MOVIES_LIST: {
                    parseMovieData(o);
                    break;
                }

                case MOVIES_LOAD: {
                    parseMovieData(o);
                    break;
                }

                case MOVIES_FILTER: {
                    parseMovieData(o);
                    break;
                }

                case MOVIE_DETAIL: {
                    try {
                        movieDetailParse = new Gson().fromJson(o.getResponseData(), MovieDetail.class);

                    } catch (JsonParseException exp) {
                        movieDetailParse = null;
                    }
                    break;
                }

            }

        } else {
            modelParsedResponse = null;
            movieDetailParse = null;
        }


    }

    private void parseMovieData(HttpResponse o) {
        try {
            modelParsedResponse = new Gson().fromJson(o.getResponseData(), ModelParse.class);
        } catch (JsonParseException exp) {
            modelParsedResponse = null;
        }
    }

    @Override
    public void onError(int type, HttpResponse o, Exception e) throws JSONException {
        spinnerStop(spinnerDialog);
        customDialogSomethingWent();
    }

    @Override
    public void noInternetAccess(int type) {
        spinnerStop(spinnerDialog);
    }

    @Override
    public void requestHttpCall(final int type, final String... params) {

        spinnerStart(spinnerDialog);

        final JsonObject jsonObject = new JsonObject();
        final HashMap map = FrontEngine.getInstance().getMap(
                new String[]{activity.getString(R.string.content_type),
                        getString(R.string.app_json)});

        CheckInternet.getInstance().internetCheckTask(activity, new CheckInternet.ConnectionCallBackInternet() {
            @Override
            public void intenetConnected(boolean status) {
                if (status) {

                    if(type == MOVIES_FILTER || type == MOVIES_LIST || type == MOVIES_LOAD){
                        FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                map,
                                params[0],
                                jsonObject,
                                new CallBackRetrofit(type,
                                        MovieActivity.this
                                ));

                    }else{
                        if(type == MOVIE_DETAIL){
                            FrontEngine.getInstance().getRetrofitFactory().requestService(RetrofitFactory.GET,
                                    map,
                                    params[0],
                                    jsonObject,
                                    new CallBackRetrofit(type,
                                            MovieActivity.this
                                    ));
                        }
                    }


                } else {
                    spinnerStop(spinnerDialog);
                    callDialog(activity, getString(R.string.alert), getString(R.string.please_connect));
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(search){
            search = false;
            searchPage = 1;
            setMovieAdapter(moviesList);
        }


    }
}
