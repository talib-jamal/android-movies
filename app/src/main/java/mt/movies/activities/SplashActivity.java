package mt.movies.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import mt.movies.R;

public class SplashActivity extends BaseActivity {

    private Handler mHandler = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        callNextActivity();

    }
    //onCreate close

    //call next activity through handler after 3second
    // movie activity swill show
    public void callNextActivity(){
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Bundle bundle = new Bundle();
                startNewActivity(SplashActivity.this,
                        MovieActivity.class,
                        Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK,
                        0,
                        0,
                        bundle,
                        false,
                        0);
                finish();

            }
        }, 3000);
    }
    //call next activity close


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);  //remove callback if back button press
        }
    }
    //onBackPressed close


}
