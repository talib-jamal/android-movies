package mt.movies.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import mt.movies.R;
import mt.movies.activities.BaseActivity;


/**
 *
 * Created by talib on 1/13/2017.
 */
public class SpinnerDialog extends Dialog {

    private Activity activity;
    private ProgressBar progressBar;

    public SpinnerDialog(BaseActivity activity) {
        super(activity, R.style.dialog);
        this.activity = activity;
    }

    public SpinnerDialog(BaseActivity activity, int style) {
        super(activity, style);
        this.activity = activity;
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullScreenCode();
        setContentView(R.layout.dialog_spinner);

        initViews();

    }

    protected void fullScreenCode() {

        Window window = this.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

    }

    private void initViews() {

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;

        if (currentapiVersion >= 14) {
            // Do something for 14 and above versions
            progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(activity ,R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        } else {
            // do something for phones running an SDK before 14
            progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(activity ,R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        }


    }




}
