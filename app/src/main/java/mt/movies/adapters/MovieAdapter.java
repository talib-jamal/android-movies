package mt.movies.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import mt.movies.R;
import mt.movies.activities.BaseActivity;
import mt.movies.interfaces.ListItemClick;
import mt.movies.models.ModelParse;
import mt.movies.models.Movies;

/**
 * Created by talib on 11/3/2016.
 */

public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Movies> arrayList;
    private Movies item = null;
    private BaseActivity activity;
    private View v = null;
    private RecyclerView recyclerView;
    private Intent intent;
    private ListItemClick listItemClick;

    public MovieAdapter(BaseActivity activity, List<Movies> arrayList, ListItemClick listItemClick) {
        this.arrayList = arrayList;
        this.activity = activity;
        this.listItemClick = listItemClick;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        v = LayoutInflater.from(activity).inflate(R.layout.adapter_movies, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {

        item = arrayList.get(position);

        final MyViewHolder holder = (MyViewHolder) viewHolder;

        holder.tvTitle.setText(""+item.getTitle());
        holder.tvSubtitle.setText(""+item.getOverview());

        activity.setImageOnGlide(activity.imagePath+item.getPosterPath(),
                holder.ivMoviePic,
                holder.pb);

        holder.listItem.setTag(position);
        holder.listItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listItemClick.itemClick(arrayList.get(Integer.parseInt(holder.listItem.getTag().toString())));
            }
        });



    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }


    //base view holder start
    private class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivMoviePic;
        private LinearLayout listItem;
        private TextView tvTitle, tvSubtitle;
        private ProgressBar pb;
        private ImageView ivLoad;

        MyViewHolder(View view) {
            super(view);

            ivMoviePic = (ImageView) view.findViewById(R.id.ivMoviePic);
            listItem = (LinearLayout)view.findViewById(R.id.listItem);
            tvTitle = (TextView)view.findViewById(R.id.tvTitle);
            tvSubtitle = (TextView)view.findViewById(R.id.tvSubtitle);
            pb = (ProgressBar)view.findViewById(R.id.pb);
            ivLoad = (ImageView)view.findViewById(R.id.ivLoad);

        }
    }
    //baseview holder close




}