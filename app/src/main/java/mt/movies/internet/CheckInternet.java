package mt.movies.internet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Talib on 8/28/2015.
 */

public class CheckInternet {

	static ConnectivityManager connectivity;
	static NetworkInfo activeNetwork;
	static CheckInternet checkInternet;
	static boolean check = false;

	//internet connection class instance start
	public static CheckInternet getInstance(){

		if(checkInternet == null){
			checkInternet = new CheckInternet();
		}
		return checkInternet;
	}
	//internet connection class instance close


	//checking internet connection is available or not start
	public void internetCheckTask(Context context, final ConnectionCallBackInternet callBackInternet){

		connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		activeNetwork = connectivity.getActiveNetworkInfo();


		if (activeNetwork != null) {

			if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI){
				check = true;
			}else{
				if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE){
					check = true;
				}
			}


		}else{
			check = false;
		}

		callBackInternet.intenetConnected(check);

	}
	//checking internet connection is available or not start

	//interface connection callback start
	public interface ConnectionCallBackInternet {
		void intenetConnected(boolean status);
	}
	//interface connection callback close
}